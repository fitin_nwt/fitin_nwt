import { FitinPage } from './app.po';

describe('fitin App', function() {
  let page: FitinPage;

  beforeEach(() => {
    page = new FitinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
