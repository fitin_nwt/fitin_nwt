import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../store.service';
import {ArticleClass as Article} from './../article-class';

@Component({
  selector: 'app-program-details',
  templateUrl: './program-details.component.html',
  styleUrls: ['./program-details.component.css']
})
export class ProgramDetailsComponent implements OnInit {

   private id: string;
   private program: Article;

  constructor(private router: Router, private storeService: StoreService) { }

  ngOnInit() {
    this.id =  this.router.url.substr(this.router.url.lastIndexOf('/') + 1);
    this.program = this.storeService.store.programs.find( (program) => program.id === this.id );
  }
}
