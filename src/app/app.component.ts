import { Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { StoreService } from './store.service';
import { AuthService } from './auth.service';
import {ArticleClass as Article} from './article-class';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private username: string;
  private password: string;

  private searchTerm: string;
  private searchByTitle: string;
  private search: Array<Article>;

  private isLogged: boolean;

  @ViewChild('lgModal') lgModal;

  private title: string = 'FITiN!';

  constructor(private router: Router, private storeService: StoreService, private auth: AuthService, private toastr: ToastsManager, private vref: ViewContainerRef) {
      this.username = '';
      this.password = '';
      this.searchTerm = 'programs';
      this.search = [];
      this.searchByTitle = '';
      this.toastr.setRootViewContainerRef(vref);
      this.isLogged = false;
  }

    ngOnInit() {
          this.isLogged = this.auth.isLoggedIn();

    }

    private searchArticle(): void {

       if (this.searchTerm === 'programs') {
            this.search = this.storeService.store.programs.filter( (article) => {
                     return (article.title.substr(0, this.searchByTitle.length).toUpperCase() === this.searchByTitle.trim().toUpperCase());
            });
       } else if ( this.searchTerm === 'cardios') {
            this.search = this.storeService.store.cardios.filter( (article) => {
                     return article.title.substr(0, this.searchByTitle.length).toUpperCase() === this.searchByTitle.trim().toUpperCase();
            });
       }

    }

  private showModal(): void {
     this.lgModal.show();
  }

  private details(id){  
      this.router.navigate([ this.searchTerm + '/' + id]);
  }

  private login(): void {
        if (this.username === 'tomoivan' && this.password === 'tomoivan') {
            this.auth.login();
            this.lgModal.hide();
            this.isLogged = true;
            this.toastr.success('Successful login.', 'Success!');
            this.username = '';
            this.password = '';
            this.router.navigate(['/dashboard']);
        } else {
             this.toastr.error('Username or password is incorrect!', 'Error!');
        }
  }

  private logout(): void {
        this.auth.logout();
        this.isLogged = false;
        this.toastr.success('Successful logout.', 'Success!');
        console.log(this.router.url);
        if (this.router.url === '/dashboard') {
            this.router.navigate(['/home']);
        }
  }
}
