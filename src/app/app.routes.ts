import { Routes } from '@angular/router';

// Components
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgramsComponent } from './programs/programs.component';
import { ProgramDetailsComponent } from './program-details/program-details.component';
import { CardioComponent } from './cardio/cardio.component';
import { CardioDetailsComponent } from './cardio-details/cardio-details.component';

export const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'programs', component: ProgramsComponent },
    { path: 'programs/:id', component: ProgramDetailsComponent },
    { path: 'cardios', component: CardioComponent },
    { path: 'cardios/:id', component: CardioDetailsComponent },
    { path: '**', component: HomeComponent }
];

