export class ArticleClass {

    public id: string;
    public title: string;
    public description: string;
    public content: string;
    public published: Date;
    public author: string;
    public picturePath: string;
    public rating: number;

    constructor(title: string, description: string, content: string, author: string, picturePath: string){


                this.id = this.createID();
                this.title = title;
                this.description = description;
                this.content = content;
                this.published = new Date();
                this.author = author;
                this.picturePath = picturePath;
                this.rating = 0;

    }

    private createID(): string {
        let text = '';
        let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for ( let i = 0; i < 5; i++ ){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }


}
