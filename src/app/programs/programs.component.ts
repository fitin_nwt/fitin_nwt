import { Component, OnInit } from '@angular/core';
import { StoreService } from './../store.service';
import {ArticleClass as Article} from './../article-class';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.css']
})
export class ProgramsComponent implements OnInit {

  private programs: Array<Article>;
  private p: number;
  private filterText: string;

  constructor(private storeService: StoreService) {

  }

  ngOnInit() {
      this.filterText = 'Filter';
      this.programs = this.storeService.store.programs;
  }

  private oldest(): void {
    
        this.filterText = 'Najstarije';
        this.programs = this.programs.sort( (a, b) => {

              if (a.published < b.published) {
                return -1;
              }
              if (a.published > b.published) {
                return 1;
              }

             return 0;
        });
  }

  private newest(): void {

          this.filterText = 'Najnovije';
          this.programs = this.programs.sort( (a, b) => {

              if (a.published < b.published) {
                return 1;
              }
              if (a.published > b.published) {
                return -1;
              }

             return 0;
        });
  }


}
